# Technical Specifications for Csaba Bernath portfolio site

## Scope

By link below you can get a PSD template with design from real client. You need to code basic skeleton and few blocks. It's your test for job position of front-end developer.

[Download PSD template (600mb)](https://drive.google.com/file/d/0Bz0xjTksaCZQQWg4VWY2eXhSVDQ/view?usp=sharing)

## Basic requirements

- Write your comments and commits only in English;
- Fork this repository on BitBucket to your account;
- Create a feature branch;
- Write the code;
- Commit all source files, not only build;
- Make pull request.

## Technology stack

- No frameworks (however, you can use some mixins and own reusable code blocks);
- If you use build system (like Gulp or Grunt) commit .json file too;
- You can use a few js libraries, but the page must be very fast;
- Your project must successfully pass w3c validation.

## Basic skeleton

- Semantic skeleton mark-up;
- Responsive, mobile-first layout;
- Basic container has border with changing color on scroll;

## Header block

 - Main navigation toggled on click;
 - Logo has two states (full on top scroll position, minimized when page scrolls down).

## Portfolio block

- Pictures have responsive animated grid layout;
- You can make filtering functional, but it's not necessary.
